import React from 'react';
import ReactDOM from 'react-dom';
import StartView from './view/StartView'
 
const title = 'React Webpack Babel Setup';
 
ReactDOM.render(<StartView />, document.getElementById('root'));

module.hot.accept();